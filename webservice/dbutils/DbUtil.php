<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbUtil
 *
 * @author Aniket.Kamthe
 */
class DbUtil {
       //put your code here
    static  $connection;
    //database connection
    public function DbConnection(){
        // Try and connect to the database, if a connection has not been established yet
        
            if(!isset($connection)) {
                // Load configuration as an array. Use the actual location of your configuration file
                $config = parse_ini_file('config/dbconfig.ini'); 
                $connection = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
                // If connection was not successful, handle the error
                if($connection === false) {
                  echo 'Connection refused :: Social App Databse connection Error !!';
            }else{
                'Connection successful';
            }
           return $connection;
        }
         
    }
    //database connection close
    public function closeConnection($con){
        //$con = $this->connectDb();
        mysqli_close($con);
        //echo "Connection closed";
    }
}
