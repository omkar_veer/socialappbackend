<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once('../utils/DbUtils.php');

$db = new DbUtils();
$con = $db->DbConnection();

$sql = "SELECT * FROM superadmin";
$result = $con->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    $responce = array("error"=>FALSE);
    while($row = $result->fetch_assoc()) {
       //echo "Password: ".$row["password"]. " " . $row["username"]. "<br>";
        $responce["error"] = FALSE ;
        $responce["user"]["superadminid"] = $row["superid"];
        $responce["user"]["username"] = $row["username"];
        $responce["user"]["fullname"] = $row["full_name"];
    }
      echo json_encode($responce);
} else {
    echo "0 results";
}
$db->closeConnection($con);