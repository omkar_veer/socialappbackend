<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppHelpers
 *
 * @author Aniket.Kamthe
 */
class AppHelpers {
    //members
     var $connection;
     var $db;
    //static $suid,$wardid;
    //put your code here
    function __construct() {
        require_once ('dbutils/DbUtil.php');
        $this->db = new DbUtil();
        $this->connection = $this->db->DbConnection();
        
    }
    function __destruct() {
        $this->db->closeConnection($this->connection);
    }
    //helper functions
    //********GET SUPER ADMIN ID***************//
    public function getSuadminid(){
        $sql = "SELECT * FROM superadmin LIMIT 1";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $suid = $row['superid'];
            }
        }else{
            echo 'Error in getSuperAdminid() function';
        }
        //return id
        return $suid;
    }
     //********GET WARD ID***************//
    public function getWardid($wid){
        $sql = "SELECT * FROM ward WHERE wardid = ".$wid.";";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $wardid = $row['wardid'];
            }
        }else{
            echo 'Error in getWardid() function';
        }
        //return id
        return $wardid;
    }
     //********GET Party ID***************//
    public function getPartyid($pid){
        $sql = "SELECT * FROM party WHERE partyid = ".$pid.";";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $partyid = $row['partyid'];
            }
        }else{
            echo 'Error in getPartyid() function';
        }
        //return id
        return $partyid;
    }
     //********GET Wachannama Jahirnama ID***************//
    public function getJahirnamaid($jid){
        $sql = "SELECT * FROM jahirnama WHERE jid = ".$jid.";";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $jwid = $row['jid'];
            }
        }else{
            echo 'Error in getJahirnamaid() function';
        }
        //return id
        return $jwid;
    } 
    //********GET District ID***************//
    public function getStateid($sid){
        $sql = "SELECT * FROM state WHERE stateid = ".$sid.";";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $stateid = $row['stateid'];
            }
        }else{
            echo 'Error in getStateid() function';
        }
        //return id
        return $stateid;
    }
    //********GET District ID***************//
    public function getDistrictid($did){
        $sql = "SELECT * FROM dist WHERE distid = ".$did.";";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $distid = $row['distid'];
            }
        }else{
            echo 'Error in getDistrictid() function';
        }
        //return id
        return $distid;
    }
    //********GET Taluka Id ID***************//
    public function getTalukaid($tid){
        $sql = "SELECT * FROM taluka WHERE talukaid = ".$tid.";";
        $result =$this->connection->query($sql);
        if(isset($result)){
            while ($row = $result->fetch_assoc()) {
                 $talukaid = $row['talukaid'];
            }
        }else{
            echo 'Error in getTalukaid() function';
        }
        //return id
        return $talukaid;
    }
    //*********GET All States List*************//
    public function getAllStates(){
        $sql = "SELECT * FROM state";
        $states = array();
        //$response = array("error"=>FALSE);
        $result = $this->connection->query($sql);
        if(isset($result)){
            while($row = $result->fetch_assoc()){
               // $response["message"] = "States retrieve successfully";
                //$response["states"][] = $row;
                $states[] = $row;
            }
            //echo json_encode($response);
            return $states;
        }else{
           // $response["error"] = TRUE;
           // $response["message"] = "Failed to retrieve State list";
           // echo json_encode($response);
        }
    }
    //*********GET All States List*************//
    public function getAllDistricts(){
        $sql = "SELECT distid, district FROM dist";
        $districts = array();
        //$response = array("error"=>FALSE);
        $result = $this->connection->query($sql);
        if(isset($result)){
            while($row = $result->fetch_assoc()){
               // $response["message"] = "Districts retrieve successfully";
               // $response["districts"][] = $row;
                $districts[] = $row;
            }
           // echo json_encode($response);
            return $districts;
        }else{
           // $response["error"] = TRUE;
           // $response["message"] = "Failed to retrieve District list";
           // echo json_encode($response);
        }
    }
    //*********GET All States List*************//
    public function getAllTaluka(){
        $sql = "SELECT talukaid,talukaname FROM taluka";
        //$response = array("error"=>FALSE);
        $talukas = array();
        $result = $this->connection->query($sql);
        if(isset($result)){
            while($row = $result->fetch_assoc()){
                //$response["message"] = "Taluka retrieve successfully";
               // $response["taluka"][] = $row;
                $talukas[] = $row;
            }
            return $talukas;
           // echo json_encode($response);
        }else{
           // $response["error"] = TRUE;
           // $response["message"] = "Failed to retrieve Taluka list";
           // echo json_encode($response);
        }
    }
    //**********Get Location Masters**********
    public function getLocationByTalukaId($talid){
        $sql = "SELECT stateid,state,distid,district,talukaid,talukaname FROM state " 
                ."INNER JOIN dist ON state.stateid = dist.state_stateid "
                ."INNER JOIN taluka ON taluka.dist_distid = dist.distid WHERE taluka.talukaid = ".$talid.";";
        $response = array("error"=>FALSE);
        $result = $this->connection->query($sql);
        if(isset($result)){
            while($row = $result->fetch_assoc()){
                $response["error"] = FALSE;
                $response["message"] = "Location Master retrieve successfully";
                $response["locations"][] = $row;
            }
                echo json_encode($response);
        }else{
                $response["error"] = TRUE;
                $response["message"] = "Location Master retrieve failed"; 
                echo json_encode($response);
        }
    }
    public function getLocationMaster(){
        $response = array("error"=>FALSE);
        $response ["taluka"][] = $this->getAllTaluka();
        $response ["districts"][] = $this->getAllDistricts();
        $response ["states"][] = $this->getAllStates();
        echo json_encode($response);
    }
     //**********************************************************//
    //****************TO RETURN WHOLE DATA each table ***********//
    //***********************************************************//
    
    //****************SUPERADMIN RECORDS JSON*******************//
    public function getSuperAdmin(){
        $sql = "SELECT `superid`, `username`, `fullname`, `email`, `phone`, `landline`, `created`, `updated`, `lastlogin` FROM `superadmin`";
        $response = array("error"=>FALSE);
        $result = $this->connection->query($sql);
        if(isset($result)){
            while($row = $result->fetch_assoc()){
                $response["message"] = "Super Admin Details Retrieve Successfully";
                $response["superadmin"][] = $row;
             }
                echo json_encode($response);
        }else{
                $response["error"] = TRUE;
                $response["errorMessage"] = "Failed to Retrieve Super Admin Details!";
                echo json_encode($response);
        }
    }
    //****************Get All Candidate List*******************//
    public function getAllCandidate(){
        $sql = "SELECT `candidateid`, `username`, `firstname`, `middlename`, `lastname`, `gender`, `dob`, `email`, `phone`, `landline`, `lane1`, `lane2`, `pincode`, `imagepath`, `created`, `updated`, `lastlogin`, `status`, `managedby`, `createdby_superid`, `ward_wardid`, `taluka_talukaid` FROM `candidate`;";
        $response = array("error"=>FALSE);
        $result = $this->connection->query($sql);
        if(isset($result)){
            $i = 0;
            while($row = $result->fetch_assoc()){
                $response["error"] = FALSE;
                $response["message"] = "Candidate Details Retrieve Successfully";
                $from = new DateTime($row["dob"]);
                $to   = new DateTime('today');
                $age = $from->diff($to)->y;
                // $age;
                $response["candidate"][] = $row;
                $i++;
             }
                echo json_encode($response);
        }else{
                $response["error"] = TRUE;
                $response["errorMessage"] = "Failed to Retrieve Candidate Details!";
                echo json_encode($response);
        }
    }

    
}
